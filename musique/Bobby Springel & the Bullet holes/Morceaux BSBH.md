# Morceaux BSBH

Playlist officielle : https://www.youtube.com/playlist?list=PLcBd4KK3633n3_2knj_iCFiwdl4BekIvk

### Mustang - The Shadows

- https://www.e-chords.com/bass/the-shadows/mustang
- https://www.youtube.com/watch?v=sY6vPlbIC40


### Sleepwalk - The Shadows

- https://www.songsterr.com/a/wsa/shadows-sleepwalk-bass-tab-s396939
- https://www.youtube.com/watch?v=FRI1INKEafI

<!-- ### Little B - The Shadows

- https://www.songsterr.com/a/wsa/shadows-little-b-bass-tab-s79406
- https://www.youtube.com/watch?v=wpEZjLi-4fI -->

### Apache - The Shadows

- https://www.youtube.com/watch?v=2izTbEyxg0A
- [PDF](./data/The_Shadows_-_Apache_bass_tab.pdf)

### Surf Rider - The Lively Ones

- https://www.youtube.com/watch?v=klbRQ4JbIzY
- https://www.youtube.com/watch?v=8P8ZrBMjYUc

### Misirlou - Dick Dale

- https://www.youtube.com/watch?v=JjaUdqAu1vs

### Batman Taboo - Messer Chups

- https://www.youtube.com/watch?v=b-jYSD8969Y

### Rumble - Link Wray

- https://www.youtube.com/watch?v=ucTg6rZJCu4

### Bullwinkle, Pt. II - The Centurians

- https://www.youtube.com/watch?v=l2aYcC5JedQ

### Wayward Nile - The Chantays

- https://www.youtube.com/watch?v=vJYYn4avRr0

### The Atlantics - Reef Break

- https://www.youtube.com/watch?v=Ya1yu-OzIx8&t=8s

### Messer Chups - magneto

- https://www.songsterr.com/a/wsa/messer-chups-magneto-bass-tab-s544403
- https://www.youtube.com/watch?v=9M4dZw37i7s

### The Shadows - Little B

- https://www.youtube.com/watch?v=wpEZjLi-4fI
- https://www.songsterr.com/a/wsa/shadows-little-b-bass-tab-s79406

### Eddy Dwayne -  Peter Gunn

- https://www.youtube.com/watch?v=XL_2elynSfI&list=PLcBd4KK3633n3_2knj_iCFiwdl4BekIvk&index=1
- https://www.youtube.com/watch?v=28QClnjA7ko

### The Eliminators - Dawn Patrol 

- https://www.youtube.com/watch?v=m7VWwMwkWS4&list=PLcBd4KK3633n3_2knj_iCFiwdl4BekIvk&index=7

### Neil LeVang - Ghost Riders In The Sky 

- https://www.youtube.com/watch?v=0SfU56IDsb4
